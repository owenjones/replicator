require './lib/replicator'

raise "usage replicator.rb source_table destination_table" if ARGV.length < 2
replicator = Replicator.new(ARGV[0], ARGV[1])
replicator.connect

# IF THERE ARE MORE RECORDS IN SOURCE TABLE
if replicator.destination_PKs_less_than_source_PKs?
  puts "#{replicator.source_table['name']} Primary Key: #{replicator.source_table['max_PK']}"
  puts "#{replicator.destination_table['name']} Primary Key: #{replicator.destination_table['max_PK']}"
  puts "Appending #{replicator.source_table['name']} to #{replicator.destination_table['name']}"

  replicator.append_to_destination_table_source_table_after(replicator.destination_table['max_PK'])
end

replicator.divide_and_conquer(replicator.source_table, replicator.destination_table, 0, replicator.destination_table['max_PK'])
