DROP TABLE IF EXISTS sourceTable;
CREATE TABLE sourceTable (id INT NOT NULL PRIMARY KEY, a INT, b VARCHAR(255), c FLOAT);
INSERT INTO sourceTable VALUES (1, 1, 'b', 2.1),(2, 115, 'b', 2.1),(3, 1, 'b', 1.1),(4, 1, 'new value', 1.1),(5, 1, 'new value', 2.1);
INSERT INTO sourceTable VALUES (6, 1, 'b', 2.1);
INSERT INTO sourceTable VALUES (7, 1, 'b', 2.1);
INSERT INTO sourceTable VALUES (8, 1, 'b', 2.1);
