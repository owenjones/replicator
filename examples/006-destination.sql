DROP TABLE IF EXISTS destinationTable;
CREATE TABLE destinationTable (id INT NOT NULL PRIMARY KEY, a INT, b VARCHAR(255), c FLOAT);
INSERT INTO destinationTable VALUES (1, 1, 'b', 1.1),(2, 110, 'b', 1.1),(3, 1, 'b', 1.1),(4, 1, 'b', 1.1);
