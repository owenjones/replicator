DROP TABLE IF EXISTS sourceTable;
CREATE TABLE sourceTable (id INT NOT NULL PRIMARY KEY, a INT, b VARCHAR(255), c FLOAT);
INSERT INTO sourceTable VALUES (1, 2, 'one', 1.1), (2, 4, 'two', 1.1), (5, 1, 'three', 1.1);
INSERT INTO sourceTable VALUES (6, 1, 'four', 4.44444);
INSERT INTO sourceTable VALUES (3, 1, 'three', 1.1);
INSERT INTO sourceTable VALUES (4, 1, 'four', 4.44444);
