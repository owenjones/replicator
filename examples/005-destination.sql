DROP TABLE IF EXISTS destinationTable;
CREATE TABLE destinationTable (id INT NOT NULL PRIMARY KEY, a INT, b VARCHAR(255), c FLOAT);
INSERT INTO destinationTable VALUES (1, 1, 'one', 1.1), (2, 1, 'two', 1.1), (5, 1, 'three', 1.1);
