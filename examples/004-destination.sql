DROP TABLE IF EXISTS destinationTable;
CREATE TABLE destinationTable (id INT NOT NULL PRIMARY KEY, a INT, b VARCHAR(255), c FLOAT);
INSERT INTO destinationTable VALUES (1, 1, 'b', 1.1),(2, 115, 'b', 1.1),(3, 1, 'b', 1.1),(4, 1, 'new value', 1.1),(5, 1, 'new value', 1.1);
INSERT INTO destinationTable VALUES (6, 2, 'b', 2.1);
INSERT INTO destinationTable VALUES (7, 2, 'b', 2.1);
INSERT INTO destinationTable VALUES (8, 1, 'b', 2.1);
