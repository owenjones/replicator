require 'rubygems'
require 'json'
require 'mysql'
require 'digest/sha1'

class Replicator

  attr_reader :source_table, :destination_table

  COLUMNS = ['a', 'b', 'c']
  PRIMARY_KEY = 'id'

  def initialize(source_table, destination_table, json_file = 'cfg.json')
    @configuration = JSON.parse(File.read(json_file))

    @source_table = Hash.new
    @destination_table = Hash.new

    @source_table['name'] = source_table
    @destination_table['name'] = destination_table
  end

  def connect
    # SHORTER CFG VARIABLES
    source_configuration = @configuration['sourceDataSource']
    destination_configuration = @configuration['destinationDataSource']

    # MYSQL CONNECTION VARIABLES
    @source_table['connection'] = Mysql::new(source_configuration['host'], source_configuration['username'], source_configuration['password'], source_configuration['database'])
    @destination_table['connection'] = Mysql::new(destination_configuration['host'], destination_configuration['username'], destination_configuration['password'], destination_configuration['database'])
    @source_table['index'] = find_table_index(@source_table)
    @destination_table['index'] = find_table_index(@destination_table)
  end

  def destination_PKs_less_than_source_PKs?
    # CALCULATES MAXIMUM PKS FOR EACH TABLE
    @source_table['max_PK'] = single_value(@source_table['connection'].query("select max(#{PRIMARY_KEY}) from #{@source_table['name']};")).to_i
    @destination_table['max_PK'] = single_value(@destination_table['connection'].query("select max(#{PRIMARY_KEY}) from #{@destination_table['name']};")).to_i

    # IF destination['max_PK'] < source['max_PK']
    # NEEDS TO SELECT COLUMNS FROM SOURCE_TABLE WHERE PK > destination['max_PK']
    @destination_table['max_PK'] < @source_table['max_PK']
  end

  def append_to_destination_table_source_table_after(pk)
    if pk < @source_table['max_PK']
      # SELECT ROWS AFTER destination['max_PK']
      new_rows = @source_table['connection'].query("select * from #{@source_table['name']} where #{PRIMARY_KEY} > #{pk};")

      new_rows.each do |row|
        # INSERT EACH ROW INTO DESTINATION TABLE
        @destination_table['connection'].query("INSERT INTO #{@destination_table['name']} VALUES (#{row.map {|c| c ? "'#{c}'" : "NULL" }.join(', ')});")
      end
    end
  end

  def divide_and_conquer(source_table, destination_table, start_PK, end_PK)
    # SELECT * FROM BOTH TABLES
    source_query = select_star(source_table, start_PK, end_PK)
    destination_query = select_star(destination_table, start_PK, end_PK)

    # USES range TO SPLIT TABLES
    range = (end_PK - start_PK) / 2

    # STORES SHA1 HASH OF BOTH TABLES IN MEMORY
    source_table_hash = ''
    destination_table_hash = ''

    # CREATE SOURCE AND DESTINATION HASHES
    source_query.each {|row| source_table_hash = Digest::SHA1.hexdigest source_table_hash + hash_row(row)}
    destination_query.each {|row| destination_table_hash = Digest::SHA1.hexdigest destination_table_hash + hash_row(row)}

    # DONE IF BOTH HASHES MATCH
    if source_table_hash == destination_table_hash
      return
    else
      # IF DOWN TO A SINGLE RESULT IT DIFFERS FROM sourceTable TO destinationTABLE SO FIX IT
      if source_query.num_rows == 1
        ((start_PK + 1)..end_PK).each {|row_pk| fix_row(row_pk) }
        return
      else
        # CHECK end_PK IF ODD NUMBER OF RESULTS SO WE CAN STILL DIVIDE BY 2
        if source_query.num_rows % 2 != 0
          return divide_and_conquer(source_table, destination_table, end_PK - 1, end_PK), divide_and_conquer(source_table, destination_table, start_PK, end_PK - 1)
        else
          # THIS RANGE INCLUDES A DIFFERENCE BETWEEN sourceTable and destinationTable
          # CONTINUE SPLITTING UNTIL WE FIND IT
          puts "Hash mismatch between #{start_PK} - #{start_PK + range} or #{start_PK + range} - #{end_PK}"
          return divide_and_conquer(source_table, destination_table, start_PK, start_PK + range), divide_and_conquer(source_table, destination_table, start_PK + range, end_PK)
        end
      end
    end
  end

  private

  def single_value(mysql_result)
    mysql_result.fetch_row[0]
  end

  def fix_row(pk)
    puts "Checking for mismatch at #{PRIMARY_KEY} #{pk}"
    source_row = select_row(@source_table, pk)
    destination_row = select_row(@destination_table, pk)

    if hash_row(source_row) != hash_row(destination_row)
      puts "Updating row at #{PRIMARY_KEY} #{pk}"
      @destination_table['connection'].query("DELETE from #{@destination_table['name']} WHERE #{PRIMARY_KEY} = #{pk};")

      if source_row.nil? == false
        @destination_table['connection'].query("INSERT INTO #{@destination_table['name']} VALUES (#{pk}, #{source_row.map {|c| c ? "'#{c}'" : "NULL" }.join(', ')});")
      end
    end
  end

  def select_star(table, start_PK, end_PK)
    table['connection'].query("SELECT * FROM #{table['name']} WHERE #{PRIMARY_KEY}>#{start_PK} AND #{PRIMARY_KEY}<=#{end_PK};")
  end

  def select_row(table, pk)
    table['connection'].query("SELECT #{COLUMNS.join(', ')} FROM #{table['name']} WHERE #{PRIMARY_KEY} = #{pk};").fetch_row()
  end

  def hash_row(result)
    # RETURNS A SHA1 HASH OF A ROW OR nil IF THE ROW IS nil
    result ? (Digest::SHA1.hexdigest result.join('#')) : nil
  end

  def find_table_index(table)
    index_result = table['connection'].query("SHOW INDEX FROM #{table['name']};")
    index = ''
    index_result.each_hash do |r|
      r['Key_name'] ? index = r['Key_name'] : index = ''
    end
    index
  end
end
