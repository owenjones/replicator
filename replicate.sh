#!/bin/bash

usage() {
    echo "Usage: $0 sourceTable destTable"
    exit 1
}

[[ $# -ne 2 ]] && usage

ruby replicator.rb $1 $2
